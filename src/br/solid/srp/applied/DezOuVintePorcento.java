package br.solid.srp.applied;

public class DezOuVintePorcento implements Calculo {

	
	public double calcula(Funcionario funcionario) {

		if (funcionario.getSalarioBase() > 5000) {
			return funcionario.getSalarioBase() * 0.5;
		} else {
			return funcionario.getSalarioBase() * 0.3;
		}
	}
	
}
