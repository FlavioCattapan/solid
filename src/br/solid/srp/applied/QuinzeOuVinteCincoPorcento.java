package br.solid.srp.applied;

public class QuinzeOuVinteCincoPorcento implements Calculo {
	
	public double calcula(Funcionario funcionario) {
		if (funcionario.getSalarioBase() < 5000) {
			return funcionario.getSalarioBase() * 0.75;
		} else {
			return funcionario.getSalarioBase() * 0.85;
		}
	}


}
