package br.solid.srp.applied;

public class RunSingleResponsability {

	// Uma classe deve possuir uma, e apenas uma responsabilidade
	// Uma classe s� deveria ter um �nico motivo para mudar.
	// Coes�o
	// Poucos pontos de mudan�a
	// Control F incicio que o c�digo ta errado
	// Mais simples
	// Maior reuso
	// Mais f�cil de ser lida
	// S� abro ela quando realmente precisar mecher nela
	public static void main(String[] args) {

		Funcionario funcionario = new Funcionario();

		CalculadorSalario calculadorSalario = new CalculadorSalario();

	}

}
