package br.solid.srp.applied;

public class Funcionario {
	
	private Cargo cargo;
	
	private Double salarioBase;

	public Double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public double calculaSalario(){
		return cargo.getCalculo().calcula(this);
	}

}
