package br.solid.srp.applied;

public interface Calculo {
	
	double calcula(Funcionario funcionario);

}
