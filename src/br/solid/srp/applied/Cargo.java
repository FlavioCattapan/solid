package br.solid.srp.applied;

public enum Cargo {

	DESENVOLVEDOR(new DezOuVintePorcento()),
	DBA(new QuinzeOuVinteCincoPorcento()),
	TESTER(new QuinzeOuVinteCincoPorcento());
	
	private Calculo calculo;
	
	Cargo(Calculo calculo){
		this.calculo = calculo;
	}

	public Calculo getCalculo() {
		return calculo;
	}

	public void setCalculo(Calculo calculo) {
		this.calculo = calculo;
	}
	
	
	
}
