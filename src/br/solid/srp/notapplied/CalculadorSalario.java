package br.solid.srp.notapplied;

import static br.solid.srp.applied.Cargo.*;

public class CalculadorSalario {

	public double calcula(Funcionario funcionario) {

		if (DESENVOLVEDOR.equals(funcionario.getCargo())) {
			return dezOuVintePorcento(funcionario);
		}

		if (DBA.equals(funcionario.getCargo())
				|| TESTER.equals(funcionario.getCargo())) {
			return quinzeOuVinteCincoPorcento(funcionario);
		}

		throw new RuntimeException("Funcionário inválido.");

	}

	private double dezOuVintePorcento(Funcionario funcionario) {

		if (funcionario.getSalarioBase() > 5000) {
			return funcionario.getSalarioBase() * 0.5;
		} else {
			return funcionario.getSalarioBase() * 0.3;
		}
	}

	private double quinzeOuVinteCincoPorcento(Funcionario funcionario) {
		if (funcionario.getSalarioBase() < 5000) {
			return funcionario.getSalarioBase() * 0.75;
		} else {
			return funcionario.getSalarioBase() * 0.85;
		}
	}

}
