package br.solid.lsp.notapplied;

public class Rectangle {

	private Integer topLeft;
    private Integer width;
    private Integer height;
	public Integer getTopLeft() {
		return topLeft;
	}
	public void setTopLeft(Integer topLeft) {
		this.topLeft = topLeft;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer area(){
	  return this.width * this.height; 
	}
	
    

}
