package br.solid.lsp.notapplied;

public class RunSubstituicaoLiskovPrinciple {
	
	public static void main(String[] args) {
	
		Rectangle rectangle = new Rectangle();
		rectangle.setWidth(5);
		rectangle.setHeight(4);
		areaVerifier(rectangle);
		Square square = new Square();
		square.setWidth(5);
		square.setHeight(4);
		areaVerifier(square);
	}
	
	private static void areaVerifier(Rectangle rectangle){
		
		if(rectangle.area() != 20){
			throw new RuntimeException();
		}
	
		
	}
	

}
