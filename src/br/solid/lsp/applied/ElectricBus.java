package br.solid.lsp.applied;

public class ElectricBus extends Vehicle {

	public void accelerate() {
		this.increaseVoltage();
		this.connectIndividualEngines();
	}

	public void increaseVoltage() {
		System.out.println("L�gica el�trica");
	}

	public void connectIndividualEngines() {
		System.out.println("Liga conex�o");
	}

}
