package br.solid.lsp.applied;

import sun.net.www.protocol.gopher.GopherClient;

public class RunLiskovSubstitutionPrinciple {

	// Classes filhas nunca deveriam infringir as defini��es de tipo da classe
	// pai.
	// Seja q(x) uma propriedade que se pode provar do objeto x do tipo T.
	// Ent�o, q(y) tamb�m � poss�vel provar para o objeto y do tipo S, sendo S
	// um subtipo de T.
	// temos que pensar nas pr� condi��es e nas p�s condi��es
	// pr�-condi��es n�o podem ser mais restritivas
	// p�s-condi��es n�o podem ser menos restritivas

	public static void main(String[] args) {

		Vehicle vehicleCar = new Car();
		go(vehicleCar);
		Vehicle vehicleBus = new ElectricBus();
		go(vehicleBus);
		
	}

	public static void go(Vehicle vehicle) {
		vehicle.startEngine();
		vehicle.accelerate();

	}

}
