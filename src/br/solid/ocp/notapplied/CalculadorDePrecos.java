package br.solid.ocp.notapplied;

public class CalculadorDePrecos {
	
	public double calcula(Compra produto){
		
		
		
		Frete correios = new Frete();
		
		// devemos evitar sequencia de if que podem crescer descontroladamente
		double desconto = 0;
		double frete = 0;
		
		if("REGRA1".equals("REGRA")){
			TabelaPrecoPadrao tabelaPrecoPadrao = new TabelaPrecoPadrao();
			desconto = tabelaPrecoPadrao.descontoPara(produto.getValor());
			frete = correios.para(produto.getCidade());
		}
		
		if("REGRA2".equals("REGRA")){
			TabelaPrecoDiferenciada tabelaPrecoDiferenciada = new TabelaPrecoDiferenciada();
			desconto = tabelaPrecoDiferenciada.descontoPara(produto.getValor());
			frete = correios.para(produto.getCidade());
		}
		
		return produto.getValor() * (1 - desconto) + frete;
		
		
	}
			
	

}
