package br.solid.ocp.applied;

public class CalculadorDePrecos {

	private TabelaDePreco tabelaDePreco;
	private ServicoDeEntrega servicoDeEntrega;

	public CalculadorDePrecos(TabelaDePreco tabelaDePreco,
			ServicoDeEntrega servicoDeEntrega) {
		super();
		this.tabelaDePreco = tabelaDePreco;
		this.servicoDeEntrega = servicoDeEntrega;
	}

	public double calcula(Compra produto) {

		// devemos evitar sequencia de if que podem crescer descontroladamente
		double desconto = 0;
		double frete = 0;

		desconto = tabelaDePreco.descontoPara(produto.getValor());
		frete = servicoDeEntrega.para(produto.getCidade());

		return produto.getValor() * (1 - desconto) + frete;

	}

}
