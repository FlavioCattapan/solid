package br.solid.ocp.applied;

public class RunOpenClosedPrinciple {
	
	//Entidades de software (classes, m�dulos, fun��es, etc) devem estar abertas para extens�o,
	// mas fechadas para modifica��o.
	// abertas para extens�o - mudar a execu��o dela ao longo do tempo a calculadora de precos
	// muda conforme a implementacao
	// fechadas para modifica��o - n�o quero entrar na classe muitas vezes para escrever c�digo
	// n�o preciso ir na calculadora para modificar a execu��o da classe
	public static void main(String[] args) {
		
		TabelaDePreco tabelaDePreco = new TabelaPrecoDiferenciada();
		ServicoDeEntrega servicoDeEntrega = new Frete();
		CalculadorDePrecos calculadorDePrecos = new CalculadorDePrecos(tabelaDePreco, servicoDeEntrega);
		
	}

}
