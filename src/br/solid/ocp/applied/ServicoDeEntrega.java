package br.solid.ocp.applied;

public interface ServicoDeEntrega {
	
	double para(String cidade);

}
