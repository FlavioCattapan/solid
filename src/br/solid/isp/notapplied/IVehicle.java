package br.solid.isp.notapplied;

public interface IVehicle {
	
	void turnOn();
	void turnOff();
	void openDor();
	void closeDor();
	

}
