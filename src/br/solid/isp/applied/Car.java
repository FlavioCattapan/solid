package br.solid.isp.applied;

public class Car implements IVehicleDoors {

	@Override
	public void turnOn() {
		System.out.println("Turn ON the car!!!!");
	}

	@Override
	public void turnOff() {
		System.out.println("Door is CLOSE!!!!");
	}

	@Override
	public void openDor() {
		System.out.println("Door is OPEN!!!!");
	}

	@Override
	public void closeDor() {
		System.out.println("Door is CLOSE!!!!");
	}

}
