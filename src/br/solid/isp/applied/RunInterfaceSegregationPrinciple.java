package br.solid.isp.applied;

public class RunInterfaceSegregationPrinciple {
	
	// Fa�a interfaces de fina granularidade, que sejam espec�ficas para quem vai utiliz�-las
	// O principio diz basicamente que o cliente n�o deve utilizar interfaces das quais n�o precise.
	// Os clientes n�o devem ser obrigados a depender de m�todos que n�o utilizam
	
	
	public static void main(String[] args) {
		
		IVehicle iVehicle = new Motorcycle();
		iVehicle.turnOn();
		iVehicle.turnOff();
		
		IVehicleDoors iVehicleDoors = new Car();
		iVehicleDoors.openDor();
		iVehicleDoors.closeDor();
		iVehicleDoors.turnOn();
		iVehicleDoors.turnOff();
		
	}
	
	

}
