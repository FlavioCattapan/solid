package br.solid.isp.applied;

public interface IVehicleDoors extends IVehicle {
	
	void openDor();
	void closeDor();

}
