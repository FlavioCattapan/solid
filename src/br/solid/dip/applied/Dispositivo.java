package br.solid.dip.applied;

public interface Dispositivo {
	
	void ligar();
	
	void desligar();

}
