package br.solid.dip.applied;

public class RunDependencyInversionPrinciple {
	
	/* 1 - High-level modules should not depend on low-level modules. 
    	   Both should depend on abstractions.
       2 - Abstractions should not depend upon details.
    	   Details should depend upon abstractions.*/
	
    /* 1 - M�dulos de alto n�vel n�o devem depender de m�dulos de baixo n�vel.
           Ambos devem depender de abstra��es.
       2 - Abstra��es n�o devem depender de detalhes. 
           Detalhes devem depender de abstra��es. */
	
	public static void main(String[] args) {
		
		
		
	}


	

}
