package br.solid.dip.applied;

public class Lampada implements Dispositivo {

	@Override
	public void ligar() {
		System.out.println("Ligar");
	}

	@Override
	public void desligar() {
		System.out.println("desligar");
	}

}
